-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 08, 2022 at 09:32 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admindashboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `image`, `price`, `status`, `created_at`) VALUES
(1, 'Iphone 13Max Pro', 'Iphone 13Max Pro, Iphone 13Max Pro, Iphone 13Max Pro, Iphone 13Max Pro', 'iphone.jpg', 100, 1, '2022-03-07 19:41:02'),
(2, 'Iphone 13 Max', 'Iphone 13 Max, Iphone 13 Max, Iphone 13 Max, Iphone 13 Max', 'iphone.jpg', 120, 1, '2022-03-07 19:41:02'),
(3, 'Iphone 12Max Pro', 'Iphone 13Max Pro, Iphone 13Max Pro, Iphone 13Max Pro, Iphone 13Max Pro', 'iphone.jpg', 200, 0, '2022-03-07 19:41:02'),
(4, 'Iphone 12 Max', 'Iphone 13 Max, Iphone 13 Max, Iphone 13 Max, Iphone 13 Max', 'iphone.jpg', 15, 1, '2022-03-07 19:41:02'),
(5, 'Iphone 11Max Pro', 'Iphone 11Max Pro, Iphone 11Max Pro, Iphone 11Max Pro, Iphone 11Max Pro', 'iphone.jpg', 5, 1, '2022-03-07 19:41:02'),
(6, 'Iphone 11 Max', 'Iphone 11 Max, Iphone 11 Max, Iphone 11 Max, Iphone 11 Max', 'iphone.jpg', 25, 1, '2022-03-07 19:41:02'),
(7, 'Iphone 10Max Pro', 'Iphone 10Max Pro, Iphone 10Max Pro, Iphone 10Max Pro, Iphone 10Max Pro', 'iphone.jpg', 30, 1, '2022-03-07 19:41:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `role` enum('admin','user') NOT NULL DEFAULT 'user',
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `activation_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `role`, `status`, `activation_token`, `created_at`, `updated_at`) VALUES
(1, 'Abo', 'Baker', 'mianabubakarbwp@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '03447179722', 'admin', 1, 'e10adc3949ba59abbe56e057f20f883e', '2022-03-07 18:28:39', '2022-03-07 19:31:20'),
(2, 'Abo', 'BakerNew', 'mianabobakerbwp@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '03018364470', 'user', 1, 'e10adc3949ba59abbe56e057f20f883e', '2022-03-07 18:28:39', '2022-03-07 19:31:20'),
(3, 'Abo', 'Baker again', 'mianabubakar_bwp@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', '03018364470', 'user', 1, 'e10adc3949ba59abbe56e057f20f883e', '2022-03-07 18:28:39', '2022-03-07 19:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `user_products`
--

DROP TABLE IF EXISTS `user_products`;
CREATE TABLE IF NOT EXISTS `user_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_products`
--

INSERT INTO `user_products` (`id`, `user_id`, `product_id`, `quantity`, `created_at`) VALUES
(1, 2, 1, 3, '2022-03-08 11:18:38'),
(2, 2, 2, 2, '2022-03-08 12:22:55'),
(3, 3, 2, 7, '2022-03-08 11:18:38'),
(4, 3, 3, 4, '2022-03-08 12:22:55');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
