<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class General extends CI_Model 
{

    public function __construct()
	{
        parent::__construct();
    }

    function save($table,$data)
	{
		$this->db->insert($table, $data);
		if($this->db->insert_id() > 0)
		{
			return $this->db->insert_id();
		} 
		else
		{
			return FALSE;
		}
    }
	
	function update($table,$where,$data)
	{
		$this->db->update($table, $data, $where);
        if ($this->db->affected_rows() > 0)
		{
            return TRUE;
        } 
		else 
		{
            return FALSE;
        }
    }

	function delete($table,$where)
	{
    	$this->db->where($where)->delete($table);
        if ($this->db->affected_rows()==1)
            return TRUE;
        return FALSE;
    }
	
	function get($table, $objArr=FALSE, $where = false, $join = array(), $fields = FALSE, $order = array())
    {
		if($fields){
			$this->db->select($fields);
		}else{
			$this->db->select('*');
		}
		$this->db->from($table);
		if(!empty($join)){
			$this->db->join($join['table'],$join['condition'],$join['type']);
		}
		
        if($where)
		{
            $this->db->where($where);
        }
        if(!empty($order))
		{
            foreach ($order as $key => $value) 
			{
                $this->db->order_by($key,$value);
            }
        }
        $query = $this->db->get();
        if($query->num_rows() > 0)
		{
			if($objArr)
			{
				return $query->result();
			}
			else
			{
				return $query->result_array();
			}
		}
        return FALSE;
    }
	
}