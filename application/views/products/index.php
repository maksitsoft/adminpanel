<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script defer="defer">var base_url = '<?php echo base_url();?>';</script>
	<!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="response-data" style="display:none;"></div>
        <div class="invalid-data" style="display:none;"></div>
        <a href="<?php echo base_url('users/dashboard'); ?>">Dashboard</a> | 
        <a href="<?php echo base_url('user/products'); ?>">Attached Products</a>
        <table border="1">
            <thead>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th>Image</th>
                <th>Price</th>
                <th>Status</th>
                <th>Quantity</th>
                <th>Date</th>
                <th>Action</th>
            </thead>
            <tbody>
                <?php foreach($products as $product){?>
                    <tr>
                        <td><?php echo $product['id'];?></td>
                        <td><?php echo $product['title'];?></td>
                        <td><?php echo $product['description'];?></td>
                        <td><img src="<?php echo base_url();?>uploads/<?php echo $product['image'];?>" width="100" /></td>
                        <td><?php echo $product['price'];?></td>
                        <td><?php echo $product['status'] == 1 ? 'Active':'Deactive';?></td>
                        <td><input type="number" class="form-control" value="0" name="quantity[<?php echo $product['id'];?>]" /></td>
                        <td><?php echo date('m-d-Y', strtotime($product['created_at']));?></td>
                        <td><button type="button" class="btn btn-success btnPick" data-id="<?php echo $product['id'];?>">Pick</button></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <script>
        //sign up - register user save
        $("body").on("click", ".btnPick", function(){
            var quantity = $(this).parent().parent().find(".form-control").val();
            if(quantity <=0){
                alert("Please enter a valid quantity!");
                return false;
            }
            var product_id = $(this).data('id');

            $.ajax({
                dataType: 'json', 
                url: base_url+'user/product_add',
                data: {product_id:product_id, quantity:quantity},
                type: 'POST', 
                beforeSend: function(){
                    $(this).attr('disabled','disabled');
                },
                success: function(data)
                {
                    $(this).removeAttr('disabled');
                    if(data.status == 'true'){
                        if(data.message)
                        {
                            $('.response-data').text(data.message);	
                            $('.response-data').show();
                            setTimeout(function() {
                                window.location.replace(base_url+data.redirect);
                            }, 1000);
                        }
                        else
                        {
                            window.location.href = data.redirect;
                        }						
                    }
                    if(data.status == 'false'){
                        $('.invalid-data').text(data.message);
                        $('.invalid-data').show();
                        setTimeout(function() {
                            $('.invalid-data').hide();
                        }, 1000);
                    }					
                },
            });
        });
    </script>

</body>
</html>