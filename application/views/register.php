<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Register</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container">
	<h1>Create A New Account!</h1>

    <?php
        if(!empty($success_msg)){
            echo '<p class="status-msg success">'.$success_msg.'</p>';
        }elseif(!empty($error_msg)){
            echo '<p class="status-msg error">'.$error_msg.'</p>';
        }
    ?>

	<div class="row">
		<form method="POST">
            <div class="form-group">
				<label>First Name *</label>
				<input type="text" name="first_name" class="form-control" placeholder="Jhon" value="<?php echo !empty($user['first_name'])?$user['first_name']:'';?>" required />
                <?php echo form_error('first_name','<p class="help-block">', '</p>')?>
			</div>
            <div class="form-group">
				<label>Last Name *</label>
				<input type="text" name="last_name" class="form-control" placeholder="Doe" value="<?php echo !empty($user['last_name'])?$user['last_name']:'';?>" />
                <?php echo form_error('last_name','<p class="help-block">', '</p>')?>
			</div>
            <div class="form-group">
				<label>Email *</label>
				<input type="email" name="email" class="form-control" placeholder="example@example.com" value="<?php echo !empty($user['email'])?$user['email']:'';?>" />
                <?php echo form_error('email','<p class="help-block">', '</p>')?>
			</div>
			<div class="form-group">
				<label>Password *</label>
				<input type="password" name="password" class="form-control" placeholder="******" required />
                <?php echo form_error('password','<p class="help-block">', '</p>')?>
			</div>
            <div class="form-group">
				<label>Confirm Password *</label>
				<input type="password" name="conf_password" class="form-control" placeholder="******" required />
                <?php echo form_error('conf_password','<p class="help-block">', '</p>')?>
			</div>
            <div class="form-group">
				<label>Phone *</label>
				<input type="text" name="phone" class="form-control" placeholder="123456789" value="<?php echo !empty($user['phone'])?$user['phone']:'';?>" required />
                <?php echo form_error('phone','<p class="help-block">', '</p>')?>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-success" name="signupSubmit" value="Register">
			</div>
		</form>
	</div>

	<p class="footer">Already have account? <a href="<?php echo base_url('login');?>">Login Here</a> </p>
</div>

</body>
</html>
