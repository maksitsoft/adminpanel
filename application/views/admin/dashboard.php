<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script defer="defer">var base_url = '<?php echo base_url();?>';</script>
	<!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="response-data" style="display:none;"></div>
        <div class="invalid-data" style="display:none;"></div>
        <a href="<?php echo base_url('admin/dashboard'); ?>">Dashboard</a> | 
        <a href="<?php echo base_url('admin/products'); ?>">Attached Products</a>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Active Users</h4>
                        <p class="card-text"><?php echo count($activeUsers);?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Active Users & Products</h4>
                        <p class="card-text"><?php echo count($activeUsersWithActiveProduct);?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Active Products</h4>
                        <p class="card-text"><?php echo count($activeProducts);?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Active Unattached Product</h4>
                        <p class="card-text"><?php echo count($product_diff);?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Active Ptoduct Price</h4>
                        <p class="card-text"><?php echo $totalActiveProductPrice;?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach($activeTotalUserProductPrice as $key => $val){?>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"><?php echo getUser($key); ?></h4>
                            <p class="card-text"><?php echo $val;?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</body>
</html>