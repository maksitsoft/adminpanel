<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		//Load form validation Library
		$this->load->library('form_validation');
		//Load User Model
		$this->load->model('user');
		//Load Geenral Model
		$this->load->model('general');
		//User login status
		$this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
	}
	
	public function index()
	{
		if($this->isUserLoggedIn){
			if($this->session->userdata('userRole') == 'admin'){
				redirect('admin/dashboard');
			}else{
				redirect('users/dashboard');
			}
		}else{
			$this->load->view('login');
		}
	}

	public function login()
	{
		$data = array();
		if($this->session->userdata('success_msg')){
			$data['success_msg'] = $this->session->userdata('success_msg');
			$this->session->unset_userdata('success_msg');
		}
		if($this->session->userdata('error_msg')){
			$data['error_msg'] = $this->session->userdata('error_msg');
			$this->session->unset_userdata('error_msg');
		}
		if($this->input->post('loginSubmit')){
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if($this->form_validation->run() == true){
				$con = array(
					'returnType' => 'single',
					'conditions' => array(
						'email' => $this->input->post('email'),
						'password' => md5($this->input->post('password')),
						'status' => 1
					),
				);

				$checkLogin = $this->user->getRows($con)[0];
				if($checkLogin){
					$this->session->set_userdata('isUserLoggedIn', TRUE);
					$this->session->set_userdata('userId', $checkLogin['id']);
					$this->session->set_userdata('userRole', $checkLogin['role']);
					if($checkLogin['role'] == 'admin'){
						redirect('admin/dashboard');
					}else{
						redirect('users/dashboard');
					}
				}else{
					$data['error_msg'] = 'Wrong email or password, please try again!';
				}
			}else{
				$data['error_msg'] = 'Please fill all required fields!';
			}
		}
		$this->load->view('login');
	}

	public function register()
	{
		$data = $userData = array();
		if($this->input->post('signupSubmit')){
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('conf_password', 'Confirm password', 'required|matches[password]');
			$this->form_validation->set_rules('phone', 'Phone', 'required');

			$userData = array(
				'first_name' => strip_tags($this->input->post('first_name')),
				'last_name' => strip_tags($this->input->post('last_name')),
				'email' => strip_tags($this->input->post('email')),
				'password' => md5($this->input->post('password')),
				'phone' => strip_tags($this->input->post('phone')),
				'activation_token' => substr(sha1(mt_rand()),17,30).time(),
			);

			if($this->form_validation->run() == true){
				if(sendEmails('Activate Your Account', $this->input->post('email'), 'activateAccount', $userData))
				{
					$this->session->set_flashdata('success', 'Please check your email to activate your account.');
					$insert = $this->user->insert($userData);
					if($insert){
						$this->session->set_flashdata('success_msg',  'Your account registred succefully verify email!');
						redirect('login');
					}else{
						$this->session->set_flashdata('error_msg', 'Some error occured please try again!');
					}
				}else{
					$this->session->set_flashdata('error_msg', 'Some error occured please try again!');
				}
			}else{
				$this->session->set_flashdata('error_msg', 'Please fill all required fields!');
			}
		}
		$data['user'] = $userData;
		$this->load->view('register', $data);
	}

	public function email_check($string){
		$con = array(
			'returnType' => 'count',
			'conditions' => array(
				'email' => $string
			),
		);

		$checkEmail = $this->user->getRows($con);
		if($checkEmail > 0){
			$this->form_validation->set_message('email_check', 'Given email already exists');
			return FALSE;
		}else{
			return TRUE;
		}
	}

	function mailConfirmation($email, $accessToken)
	{
		$con = array(
			'returnType' => 'count',
			'conditions' => array(
				'email' => $email,
				'activation_token' => $accessToken
			),
		);
		$result = $this->user->getRows($con);
		if($result > 0)
		{
			$updateData = array
			(
				'activation_token' => substr(sha1(mt_rand()),17,30).time(),
				'status' => 1,
			);
			$where = array('email' => $email);
			$result = $this->general->update('users', $where, $updateData);
			if($result == TRUE)
			{
				$this->session->set_flashdata('success_msg', 'Your accounr has been Activated successfully. Please Login!.');
				redirect('login');
			}
		}
		else
		{
			$this->session->set_flashdata('error_msg', 'Access Token Expired!!.');
			redirect('signup');
		}
	}
}
