<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		//Load form validation Library
		$this->load->library('form_validation');
		//Load User Model
		$this->load->model('user');
		$this->load->model('general');
		//User login status
		$this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
        if($this->isUserLoggedIn){
            if($this->session->userdata('userRole') != 'admin'){
                redirect('users/dashboard');
            }else{
            }
        }else{
            redirect('login');
        }
	}

    public function index(){
		$products = $user_products = $user_active_prod = array();
		$total = 0;
		$where = array('role' => 'user', 'status' => 1);
		$data['activeUsers'] = $this->general->get('users', false, $where);
		$fields = 'p.*, user_products.user_id, user_products.product_id, user_products.quantity';
		$join = array(
			'table' => 'products as p',
			'condition' => 'p.id = user_products.product_id',
			'type' => 'INNER',
		);
		$data['activeUsersWithActiveProduct'] = $this->general->get('user_products', false, false, $join, $fields);
		$where = array('status' => 1);
		$data['activeProducts'] = $this->general->get('products', false, $where);

		foreach($data['activeProducts'] as $val){
			$products[] = $val['id'];
		}

		foreach($data['activeUsersWithActiveProduct'] as $val){
			if($val['status']){
				$data['activeProductPrice'][] = $val['price']*$val['quantity'];
				$data['activeUserProductPrice'][$val['user_id']][] = $val['price']*$val['quantity'];
			}
			$user_products[] = $val['product_id'];
		}
		foreach($data['activeProductPrice'] as $val){
			$total = $total + $val;
		}

		foreach($data['activeUserProductPrice'] as $key => $val){
			$data['activeTotalUserProductPrice'][$key] = array_sum($val);
		}
		$data['totalActiveProductPrice'] = $total;

		$data['product_diff'] = array_diff($products,$user_products);

		return $this->load->view('admin/dashboard', $data);
    }

	public function products(){
        $data['products'] = $this->general->get('products');
        return $this->load->view('products/index', $data);
    }
}
