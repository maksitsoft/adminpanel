<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		//Load form validation Library
		$this->load->library('form_validation');
		//Load User Model
		$this->load->model('user');
		$this->load->model('general');
		//User login status
		$this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
        if($this->isUserLoggedIn){
            if($this->session->userdata('userRole') != 'user'){
                redirect('admin/dashboard');
            }
        }else{
            redirect('login');
        }
	}

    public function index(){
        $fields = 'p.*, user_products.user_id, user_products.product_id, user_products.quantity, user_products.created_at as up_created';
        $where = array('user_id' => $this->session->userdata('userId'));
        $join = array(
            'table' => 'products as p',
            'condition' => 'p.id = user_products.product_id',
            'type' => 'INNER',
        );
        $data['products'] = $this->general->get('user_products', false, $where, $join, $fields);
        return $this->load->view('user/products', $data);
    }
}
