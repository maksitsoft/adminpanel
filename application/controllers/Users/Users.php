<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		//Load form validation Library
		$this->load->library('form_validation');
		//Load User Model
		$this->load->model('user');
		$this->load->model('general');
		//User login status
		$this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
        if($this->isUserLoggedIn){
            if($this->session->userdata('userRole') != 'user'){
                redirect('admin/dashboard');
            }
        }else{
            redirect('login');
        }
	}

    public function dashboard(){
        $data['products'] = $this->general->get('products');
        return $this->load->view('products/index', $data);
    }

    public function product_add(){
        $product_data = array(
            'user_id' => $this->session->userdata('userId'),
            'product_id' => $this->input->post('product_id'),
            'quantity' => $this->input->post('quantity')
        );
        $response = $this->general->save('user_products', $product_data);
        if($response){
            $result = array(
                'status' => 'true',
                'message' => 'Product added successfully!',
                'redirect' => 'user/products'
            );
        }else{
            $result = array(
                'status' => 'false',
                'message' => 'Some error occured!'
            );
        }
        echo json_encode($result);
        exit;
    }
}