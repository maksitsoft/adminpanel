<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    if ( ! function_exists('getUser')){
		function getUser($id)
		{
			$CI = &get_instance();
			
			$CI->db->where('id', $id);
			$result = $CI->db->get('users')->row();
			if($result)
			{			
                return $result->first_name.' '.$result->last_name;
            }
            else
            {
                return FALSE;
            }
		}
    }